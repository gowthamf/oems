namespace OEMSWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseId = c.Int(nullable: false, identity: true),
                        CourseName = c.String(),
                    })
                .PrimaryKey(t => t.CourseId);
            
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        ModuleId = c.Int(nullable: false, identity: true),
                        ModuleName = c.String(),
                        CourseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ModuleId)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Lectures",
                c => new
                    {
                        LectureId = c.Int(nullable: false, identity: true),
                        LectureName = c.String(),
                    })
                .PrimaryKey(t => t.LectureId);
            
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        MarksId = c.Int(nullable: false, identity: true),
                        MarksGrade = c.Double(nullable: false),
                        Modules_ModuleId = c.Int(),
                        Student_StudentId = c.Int(),
                    })
                .PrimaryKey(t => t.MarksId)
                .ForeignKey("dbo.Modules", t => t.Modules_ModuleId)
                .ForeignKey("dbo.Students", t => t.Student_StudentId)
                .Index(t => t.Modules_ModuleId)
                .Index(t => t.Student_StudentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentId = c.Int(nullable: false, identity: true),
                        StudentName = c.String(),
                        Courses_CourseId = c.Int(),
                    })
                .PrimaryKey(t => t.StudentId)
                .ForeignKey("dbo.Courses", t => t.Courses_CourseId)
                .Index(t => t.Courses_CourseId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        QuestionId = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                    })
                .PrimaryKey(t => t.QuestionId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        Lecture_LectureId = c.Int(),
                        Student_StudentId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Lectures", t => t.Lecture_LectureId)
                .ForeignKey("dbo.Students", t => t.Student_StudentId)
                .Index(t => t.Lecture_LectureId)
                .Index(t => t.Student_StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Student_StudentId", "dbo.Students");
            DropForeignKey("dbo.Users", "Lecture_LectureId", "dbo.Lectures");
            DropForeignKey("dbo.Marks", "Student_StudentId", "dbo.Students");
            DropForeignKey("dbo.Students", "Courses_CourseId", "dbo.Courses");
            DropForeignKey("dbo.Marks", "Modules_ModuleId", "dbo.Modules");
            DropForeignKey("dbo.Modules", "CourseId", "dbo.Courses");
            DropIndex("dbo.Users", new[] { "Student_StudentId" });
            DropIndex("dbo.Users", new[] { "Lecture_LectureId" });
            DropIndex("dbo.Students", new[] { "Courses_CourseId" });
            DropIndex("dbo.Marks", new[] { "Student_StudentId" });
            DropIndex("dbo.Marks", new[] { "Modules_ModuleId" });
            DropIndex("dbo.Modules", new[] { "CourseId" });
            DropTable("dbo.Users");
            DropTable("dbo.Questions");
            DropTable("dbo.Students");
            DropTable("dbo.Marks");
            DropTable("dbo.Lectures");
            DropTable("dbo.Modules");
            DropTable("dbo.Courses");
        }
    }
}
