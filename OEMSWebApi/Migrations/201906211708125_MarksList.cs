namespace OEMSWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MarksList : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Marks", "Modules_ModuleId", "dbo.Modules");
            DropIndex("dbo.Marks", new[] { "Modules_ModuleId" });
            RenameColumn(table: "dbo.Marks", name: "Modules_ModuleId", newName: "ModuleId");
            AlterColumn("dbo.Marks", "ModuleId", c => c.Int(nullable: false));
            CreateIndex("dbo.Marks", "ModuleId");
            AddForeignKey("dbo.Marks", "ModuleId", "dbo.Modules", "ModuleId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Marks", "ModuleId", "dbo.Modules");
            DropIndex("dbo.Marks", new[] { "ModuleId" });
            AlterColumn("dbo.Marks", "ModuleId", c => c.Int());
            RenameColumn(table: "dbo.Marks", name: "ModuleId", newName: "Modules_ModuleId");
            CreateIndex("dbo.Marks", "Modules_ModuleId");
            AddForeignKey("dbo.Marks", "Modules_ModuleId", "dbo.Modules", "ModuleId");
        }
    }
}
