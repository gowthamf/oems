﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OEMSWebApi.Models
{
    public class DataContext:DbContext
    {
        public DataContext():base("OEMSConnection")
        {

        }

        public DbSet<Users> Users { get; set; }

        public DbSet<Courses> Courses { get; set; }

        public DbSet<Modules> Modules { get; set; }

        public DbSet<Questions> Questions { get; set; }

        public DbSet<Lectures> Lectures { get; set; }

        public DbSet<Marks> Marks { get; set; }

        public DbSet<Student> Students { get; set; }
    }
}