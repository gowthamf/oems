﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OEMSWebApi.Models
{
    public class Courses
    {
        [Key]
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public List<Modules> ModulesList { get; set; }
    }
}