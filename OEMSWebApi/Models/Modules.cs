﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OEMSWebApi.Models
{
    public class Modules
    {
        [Key]
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int CourseId { get; set; }

        public Courses Courses { get; set; }

        public List<Marks> Marks { get; set; }
    }
}