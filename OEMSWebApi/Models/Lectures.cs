﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OEMSWebApi.Models
{
    public class Lectures
    {
        [Key]
        public int LectureId { get; set; }

        public string LectureName { get; set; }
    }
}