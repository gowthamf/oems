﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace OEMSWebApi.Controllers
{
    public class StudentController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Students/GetStudents")]
        public List<Student> GetStudents()
        {
            return _dbContext.Students.Include(s => s.Marks).Include(s=>s.Courses).ToList();
        }

        [HttpPut]
        [Route("api/Students/UpdateStudent/{id}")]
        public IHttpActionResult UpdateStudent(int id, Student student)
        {
            var dbStudent = _dbContext.Students.Where(s => s.StudentId == id).FirstOrDefault();

            dbStudent.StudentName = student.StudentName;

            return Ok(dbStudent);
        }

        [HttpPost]
        [Route("api/Students/CreateStudent")]
        public IHttpActionResult CreateStudent(Student student)
        {
            _dbContext.Students.Add(student);
            _dbContext.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + student.StudentId), student);
        }
    }
}
