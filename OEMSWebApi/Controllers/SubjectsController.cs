﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace OEMSWebApi.Controllers
{
    public class SubjectsController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Subjects/GetSubjects")]
        public List<Modules> GetModules()
        {
            return _dbContext.Modules.Include(m => m.Marks).Include(m=>m.Courses).ToList();
        }

        [HttpPost]
        [Route("api/Subjects/AddSubjects")]
        public IHttpActionResult AddModules(Modules modules)
        {
            _dbContext.Modules.Add(modules);

            _dbContext.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + modules.ModuleId), modules);
        }
    }
}
