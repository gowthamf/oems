﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OEMSWebApi.Controllers
{
    public class LecturesController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Lectures/GetAllLectures")]
        public List<Lectures> GetLectures()
        {
            return _dbContext.Lectures.ToList();
        }

        [HttpPut]
        [Route("api/Lectures/UpdateLecture/{id}")]
        public IHttpActionResult UpdateLectures(int id, Lectures lecture)
        {
            var dblecture = _dbContext.Lectures.Where(l => l.LectureId == id).FirstOrDefault();

            if(dblecture==null)
            {
                return NotFound();
            }

            dblecture.LectureName = lecture.LectureName;
            _dbContext.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [Route("api/Lectures/CreateLectures")]
        public IHttpActionResult CreateLectures(Lectures lectures)
        {
            _dbContext.Lectures.Add(lectures);
            _dbContext.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + lectures.LectureId), lectures);
        }
    }
}
