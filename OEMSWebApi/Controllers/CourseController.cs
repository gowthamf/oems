﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace OEMSWebApi.Controllers
{
    public class CourseController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Course/GetAllCourse")]
        public List<Courses> GetCourses()
        {
           return _dbContext.Courses.Include(c=>c.ModulesList).ToList();
        }

        [HttpPost]
        [Route("api/Course/AddCourse")]
        public IHttpActionResult AddCourse(Courses course)
        {
            _dbContext.Courses.Add(course);
            _dbContext.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + course.CourseId), course);
        }

        [HttpDelete]
        [Route("api/Course/RemoveCourse/{id}")]
        public IHttpActionResult RemoveCourse(int id)
        {
            var dbCourse = _dbContext.Courses.Where(c => c.CourseId == id).FirstOrDefault();

            _dbContext.Courses.Remove(dbCourse);

            return Ok();
        }

        [HttpPut]
        [Route("api/Course/UpdateCoure/{id}")]
        public IHttpActionResult UpdateCourse(int id, Courses course)
        {
            var dbCourse = _dbContext.Courses.Where(c => c.CourseId == id).FirstOrDefault();

            dbCourse.CourseName = course.CourseName;

            _dbContext.SaveChanges();

            return Ok();
        }
    }
}
