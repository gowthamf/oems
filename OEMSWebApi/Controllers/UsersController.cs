﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OEMSWebApi.Controllers
{
    public class UsersController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Users/Login/{userName}/{password}")]
        public Users Login(string userName, string password)
        {
            var user = _dbContext.Users.Where(u => u.UserName == userName && u.Password == password).FirstOrDefault();

            return user;
        }

        [HttpPost]
        [Route("api/Users/Register")]
        public IHttpActionResult Register(Users user)
        {
            _dbContext.Users.Add(user);

            _dbContext.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + user.UserId), user);
        }
    }
}
