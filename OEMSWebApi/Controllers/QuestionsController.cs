﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OEMSWebApi.Controllers
{
    public class QuestionsController : ApiController
    {
        DataContext _dbContext = new DataContext();

        [HttpGet]
        [Route("api/Questions/GetAllQuestions")]
        public List<Questions> GetQuestions()
        {
            return _dbContext.Questions.ToList();
        }

        [HttpPut]
        [Route("api/Questions/UpdateQuestions/{id}")]
        public IHttpActionResult UpdateQuestions(int id,Questions questions)
        {
            var question = _dbContext.Questions.Where(q => q.QuestionId == id).FirstOrDefault();

            if(question==null)
            {
                return NotFound();
            }

            question.Question = questions.Question;
            _dbContext.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        [Route("api/Questions/DeleteQuestion/{id}")]
        public IHttpActionResult DeleteQuestions(int id)
        {
            var question = _dbContext.Questions.Where(q => q.QuestionId == id).FirstOrDefault();

            if (question == null)
            {
                return NotFound();
            }

            _dbContext.Questions.Remove(question);
            _dbContext.SaveChanges();

            return Ok();
        }
    }
}
