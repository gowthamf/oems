﻿using OEMSUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OEMSUI
{
    /// <summary>
    /// Interaction logic for EditPage.xaml
    /// </summary>
    public partial class EditPage : Window
    {
        private Object _editObj = null;
        private APIClient _client = new APIClient();

        public EditPage()
        {
            //InitializeComponent();
        }

        public EditPage(Object obj)
        {
            InitializeComponent();
            var nameOfClass = obj.GetType().Name;
            RowDefinition[] rowDefinitions = new RowDefinition[4];
            ColumnDefinition[] columnDefinitions = new ColumnDefinition[2];

            if (nameOfClass == "Questions")
            {
                _editObj = obj;
                dynamicGrid.Children.Clear();
                var questionObj = (Questions)obj;

                TextBlock textBlock = new TextBlock();
                textBlock.Margin = new Thickness(20, 20, 0, 0);
                textBlock.Text = "Question";
                Grid.SetRow(textBlock, 2);
                Grid.SetColumn(textBlock, 1);
                dynamicGrid.Children.Add(textBlock);


                TextBox textBox = new TextBox();
                textBox.Margin = new Thickness(120, 20, 0, 0);
                textBox.TextWrapping = TextWrapping.Wrap;
                textBox.Text = questionObj.Question;
                Grid.SetRow(textBox, 2);
                Grid.SetColumn(textBox, 1);
                dynamicGrid.Children.Add(textBox);
            }
            else if(nameOfClass=="Student")
            {
                _editObj = obj;
                dynamicGrid.Children.Clear();
                var studentObj = (Student)obj;

                TextBlock textBlock = new TextBlock();
                textBlock.Margin = new Thickness(20, 20, 0, 0);
                textBlock.Text = "Student Name";
                Grid.SetRow(textBlock, 2);
                Grid.SetColumn(textBlock, 1);
                dynamicGrid.Children.Add(textBlock);


                TextBox textBox = new TextBox();
                textBox.Margin = new Thickness(120, 20, 0, 0);
                textBox.TextWrapping = TextWrapping.Wrap;
                textBox.Text = studentObj.StudentName;
                Grid.SetRow(textBox, 2);
                Grid.SetColumn(textBox, 1);
                dynamicGrid.Children.Add(textBox);
            }
            else if(nameOfClass=="Courses")
            {
                _editObj = obj;
                dynamicGrid.Children.Clear();
                var courseObj = (Courses)obj;

                TextBlock textBlock = new TextBlock();
                textBlock.Margin = new Thickness(20, 20, 0, 0);
                textBlock.Text = "Course Name";
                Grid.SetRow(textBlock, 2);
                Grid.SetColumn(textBlock, 1);
                dynamicGrid.Children.Add(textBlock);


                TextBox textBox = new TextBox();
                textBox.Margin = new Thickness(120, 20, 0, 0);
                textBox.TextWrapping = TextWrapping.Wrap;
                textBox.Text = courseObj.CourseName;
                Grid.SetRow(textBox, 2);
                Grid.SetColumn(textBox, 1);
                dynamicGrid.Children.Add(textBox);
            }
            else if(nameOfClass=="Lectures")
            {
                _editObj = obj;
                dynamicGrid.Children.Clear();
                var lectureObj = (Lectures)obj;

                TextBlock textBlock = new TextBlock();
                textBlock.Margin = new Thickness(20, 20, 0, 0);
                textBlock.Text = "Lecture Name";
                Grid.SetRow(textBlock, 2);
                Grid.SetColumn(textBlock, 1);
                dynamicGrid.Children.Add(textBlock);


                TextBox textBox = new TextBox();
                textBox.Margin = new Thickness(120, 20, 0, 0);
                textBox.TextWrapping = TextWrapping.Wrap;
                textBox.Text = lectureObj.LectureName;
                Grid.SetRow(textBox, 2);
                Grid.SetColumn(textBox, 1);
                dynamicGrid.Children.Add(textBox);
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if(_editObj.GetType().Name=="Questions")
            {
                var dynamicChildren = dynamicGrid.Children;
                var textBoxes = dynamicChildren.OfType<TextBox>();
                var questionObj = (Questions)_editObj;
                foreach (var item in textBoxes)
                {
                    questionObj.Question = item.Text;
                }
                await _client.UpdateQuestions(questionObj.QuestionId, questionObj);
            }
            else if(_editObj.GetType().Name== "Lecture")
            {
                var dynamicChildren = dynamicGrid.Children;
                var textBoxes = dynamicChildren.OfType<TextBox>();
                var lectureObj = (Lectures)_editObj;
                foreach (var item in textBoxes)
                {
                    lectureObj.LectureName = item.Text;
                }

                await _client.UpdateLectures(lectureObj.LectureId, lectureObj);
            }
            else if(_editObj.GetType().Name== "Courses")
            {
                var dynamicChildren = dynamicGrid.Children;
                var textBoxes = dynamicChildren.OfType<TextBox>();
                var courseObj = (Courses)_editObj;
                foreach (var item in textBoxes)
                {
                    courseObj.CourseName = item.Text;
                }

                await _client.UpdateCourse(courseObj.CourseId, courseObj);
            }
            else if(_editObj.GetType().Name=="Student")
            {
                var dynamicChildren = dynamicGrid.Children;
                var textBoxes = dynamicChildren.OfType<TextBox>();
                var studentObj = (Student)_editObj;
                foreach (var item in textBoxes)
                {
                    studentObj.StudentName = item.Text;
                }

                await _client.UpdateStudent(studentObj.StudentId, studentObj);
            }
        }
    }
}
