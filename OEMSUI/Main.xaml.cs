﻿using OEMSUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OEMSUI
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        private List<Questions> QuestionsList;
        private List<Student> StudentList;
        private List<Courses> CoursesList;
        private List<Modules> ModuleList;
        private List<Lectures> LecturesList;

        APIClient client = new APIClient();

        private bool _isCourseSelected = false;
        private bool _isQuestionSelected = false;
        private bool _isStudentSelected = false;
        private bool _isModulesSelected = false;
        private bool _isLectureSelected = false;


        public Main()
        {
            //InitializeComponent();
        }

        public Main(Users user)
        {
            InitializeComponent();
            if (user.Lecture != null)
            {
                tabCourses.Visibility = Visibility.Hidden;
                tabSubjects.Visibility = Visibility.Hidden;
                tabLectures.Visibility = Visibility.Hidden;
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            var selectedQuestions = listBox.SelectedItem;
            if (selectedQuestions != null)
            {
                EditPage editPage = new EditPage(selectedQuestions);
                editPage.ShowDialog();
                await GetQuestions();
            }
        }

        private async void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedQuestions = (Questions)listBox.SelectedItem;
            if (selectedQuestions != null)
            {
                listBox.ItemsSource = await client.DeleteQuestions(selectedQuestions.QuestionId);
            }
        }

        private async void ListBoxLecture_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private async void BtnCreateCourse_Click(object sender, RoutedEventArgs e)
        {
            var modulesList = await client.GetModules();
            Register register = new Register("Course", null, modulesList);
            register.ShowDialog();
        }

        private async void BtnEditCourse_Click(object sender, RoutedEventArgs e)
        {
            var selectedCourse = listBoxCourse.SelectedItem;

            if(selectedCourse!=null)
            {
                EditPage editPage = new EditPage(selectedCourse);
                editPage.ShowDialog();
                await GetCourese();
            }
        }

        private async void BtnDeleteCourse_Click(object sender, RoutedEventArgs e)
        {
            var selectedCourse = (Courses)listBoxCourse.SelectedItem;
            if(selectedCourse!=null)
            {
                listBoxCourse.ItemsSource = await client.DeleteCourse(selectedCourse.CourseId);
            }
        }

        private async void BtnCreateSubjects_Click(object sender, RoutedEventArgs e)
        {
            var courseList = await client.GetCourses();
            Register register = new Register("Module", courseList, null);
            register.ShowDialog();
        }

        private async void BtnCreateLecture_Click(object sender, RoutedEventArgs e)
        {
            var courseList = await client.GetCourses();
            Register register = new Register("Lecture", courseList, null);
            register.ShowDialog();
        }

        private async void BtnEditLecture_Click(object sender, RoutedEventArgs e)
        {
            var selectedLecture = listBoxLecture.SelectedItem;

            if (selectedLecture != null)
            {
                EditPage editPage = new EditPage(selectedLecture);
                editPage.ShowDialog();
                await GetLectures();
            }
        }

        private async Task GetQuestions()
        {
            var questionList = await client.GetQuestionsAsync();
            listBox.ItemsSource = questionList;
            QuestionsList = questionList;
        }

        private async Task GetStudents()
        {
            listBoxStudent.ItemsSource = null;
            var studentList = await client.GetStudents();
            listBoxStudent.ItemsSource = studentList;
            StudentList = studentList;
            listBoxMarks.ItemsSource = null;
        }

        private async Task GetCourese()
        {
            listBoxCourse.ItemsSource = null;
            var courseList = await client.GetCourses();
            listBoxCourse.ItemsSource = courseList;
            CoursesList = courseList;
            listBoxSelectedModules.ItemsSource = null;

        }

        private async Task GetModules()
        {
            listBoxModules.ItemsSource = null;
            var moduleList = await client.GetModules();
            var courseList = await client.GetCourses();
            listBoxModules.ItemsSource = moduleList;
            listBoxFilter.ItemsSource = courseList;
            ModuleList = moduleList;
        }

        private async Task GetLectures()
        {
            listBoxLecture.ItemsSource = null;
            var lectureList = await client.GetLectures();
            listBoxLecture.ItemsSource = lectureList;
            LecturesList = lectureList;

        }

        private void TxtQuestions_TextChanged(object sender, TextChangedEventArgs e)
        {


            if (txtQuestions.Text == null)
            {
                listBox.ItemsSource = QuestionsList;
            }

            var filtered = QuestionsList.Where(q => q.Question.Contains(txtQuestions.Text));
            listBox.ItemsSource = filtered;

        }

        private async void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedTab = (TabItem)tabControl.SelectedItem;

            if (selectedTab != null)
            {
                if (selectedTab.Name == "tabStudents" && !_isStudentSelected)
                {
                    await GetStudents();
                    _isStudentSelected = true;
                    _isCourseSelected = false;
                    _isQuestionSelected = false;
                    _isModulesSelected = false;
                    _isLectureSelected = false;
                }
                else if (selectedTab.Name == "tabCourses" && !_isCourseSelected)
                {
                    await GetCourese();
                    _isCourseSelected = true;
                    _isQuestionSelected = false;
                    _isModulesSelected = false;
                    _isLectureSelected = false;
                    _isStudentSelected = false;
                }
                else if (selectedTab.Name == "tabQuestions" && !_isQuestionSelected)
                {
                    await GetQuestions();
                    _isQuestionSelected = true;
                    _isModulesSelected = false;
                    _isLectureSelected = false;
                    _isStudentSelected = false;
                    _isCourseSelected = false;
                }
                else if (selectedTab.Name == "tabSubjects" && !_isModulesSelected)
                {
                    await GetModules();
                    _isModulesSelected = true;
                    _isLectureSelected = false;
                    _isStudentSelected = false;
                    _isCourseSelected = false;
                    _isQuestionSelected = false;

                }
                else if(selectedTab.Name== "tabLectures" && !_isLectureSelected)
                {
                    await GetLectures();
                    _isLectureSelected = true;
                    _isStudentSelected = false;
                    _isCourseSelected = false;
                    _isQuestionSelected = false;
                    _isModulesSelected = false;
                }

            }
        }

        private void ListBoxStudent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listBoxStudent.ItemsSource!=null)
            {
                var selectedStudent = (Student)listBoxStudent.SelectedItem;

                if(selectedStudent!=null)
                {
                    var marks = selectedStudent.Marks;

                    listBoxMarks.ItemsSource = marks;
                }
            }
            
        }

        private void TxtStudents_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filtered = StudentList.Where(s => s.StudentName.Contains(txtStudents.Text));

            if(txtStudents.Text==null)
            {
                listBoxStudent.ItemsSource = StudentList;
            }

            listBoxStudent.ItemsSource = filtered;
        }

        private void ListBoxCourse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listBoxCourse.ItemsSource!=null)
            {
                var selectedCourse = (Courses)listBoxCourse.SelectedItem;

                var modules = selectedCourse.ModulesList;

                listBoxSelectedModules.ItemsSource = modules;
            }
            
        }

        private async void BtnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            var courseList = await client.GetCourses();
            Register register = new Register("Student",courseList,null);
            register.ShowDialog();
        }

        private async void BtnEditStudent_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = listBoxStudent.SelectedItem;
            if (selectedStudent != null)
            {
                EditPage editPage = new EditPage(selectedStudent);
                editPage.ShowDialog();
                await GetStudents();
            }
        }

        private void ListBoxFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (listBoxModules.ItemsSource!=null)
            {
                var selectedCourse = (Courses)listBoxFilter.SelectedItem;

                var modules = ModuleList.Where(m => m.CourseId == selectedCourse.CourseId);

                listBoxModules.ItemsSource = modules;
            }
        }

        private void TxtCourse_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filtered = CoursesList.Where(c => c.CourseName.Contains(txtCourse.Text));

            if (txtCourse.Text == null)
            {
                listBoxCourse.ItemsSource = CoursesList;
            }

            listBoxCourse.ItemsSource = filtered;
        }

        private void TxtSubjects_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filtered = ModuleList.Where(m => m.ModuleName.Contains(txtSubjects.Text));

            if(txtSubjects.Text==null)
            {
                listBoxModules.ItemsSource = ModuleList;
            }

            listBoxModules.ItemsSource = filtered;
        }

        private void TxtLectures_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filtered = LecturesList.Where(l => l.LectureName.Contains(txtLectures.Text));

            if(txtLectures.Text==null)
            {
                listBoxLecture.ItemsSource = LecturesList;
            }

            listBoxLecture.ItemsSource = filtered;
        }
    }
}
