﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Lectures
    {
        
        public int LectureId { get; set; }

        public string LectureName { get; set; }
    }
}
