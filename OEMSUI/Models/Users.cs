﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Users
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public Lectures Lecture { get; set; }

        public Student Student { get; set; }
    }
}
