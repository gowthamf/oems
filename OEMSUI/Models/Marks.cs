﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Marks
    {
        public int MarksId { get; set; }

        public double MarksGrade { get; set; }

        public Student Student { get; set; }

        public Modules Modules { get; set; }

        public int ModuleId { get; set; }
    }
}
