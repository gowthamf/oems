﻿using OEMSWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Courses
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public List<Modules> ModulesList { get; set; }
    }
}
