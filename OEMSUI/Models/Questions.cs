﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Questions
    {
        
        public int QuestionId { get; set; }

        public string Question { get; set; }
    }
}
