﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class APIClient
    {
        HttpClient client = new HttpClient();
        public APIClient()
        {
            client.BaseAddress = new Uri("http://localhost:50263/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Users> LoginAsync(string userName, string password)
        {
            Users user = null;
            HttpResponseMessage response = await client.GetAsync($"api/Users/Login/{userName}/{password}");
            if (response.IsSuccessStatusCode)
            {
                user = await response.Content.ReadAsAsync<Users>();
            }
            return user;
        }

        public async Task<List<Questions>> GetQuestionsAsync()
        {
            List<Questions> questions = null;

            HttpResponseMessage responseMessage = await client.GetAsync("api/Questions/GetAllQuestions");

            if(responseMessage.IsSuccessStatusCode)
            {
                questions = await responseMessage.Content.ReadAsAsync<List<Questions>>();
            }

            return questions;
        }

        public async Task<List<Questions>> DeleteQuestions(int id)
        {
            List<Questions> questions = null;

            HttpResponseMessage responseMessage = await client.DeleteAsync($"api/Questions/DeleteQuestion/{id}");

            if(responseMessage.IsSuccessStatusCode)
            {
                HttpResponseMessage httpResponseMessage= await client.GetAsync("api/Questions/GetAllQuestions");
                questions = await httpResponseMessage.Content.ReadAsAsync<List<Questions>>();
            }

            return questions;
        }

        public async Task<bool> UpdateQuestions(int id, Questions question)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/Questions/UpdateQuestions/{id}",question);

            if(responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<List<Student>> GetStudents()
        {
            List<Student> students = null;

            HttpResponseMessage responseMessage= await client.GetAsync("api/Students/GetStudents");

            if(responseMessage.IsSuccessStatusCode)
            {
                students = await responseMessage.Content.ReadAsAsync<List<Student>>();
            }

            return students;
        }

        public async Task<List<Courses>> GetCourses()
        {
            List<Courses> courses = null;

            HttpResponseMessage responseMessage = await client.GetAsync("api/Course/GetAllCourse");

            if(responseMessage.IsSuccessStatusCode)
            {
                courses = await responseMessage.Content.ReadAsAsync<List<Courses>>();
            }

            return courses;
        }

        public async Task<List<Modules>> GetModules()
        {
            List<Modules> modules = null;

            HttpResponseMessage responseMessage = await client.GetAsync("api/Subjects/GetSubjects");

            if(responseMessage.IsSuccessStatusCode)
            {
                modules = await responseMessage.Content.ReadAsAsync<List<Modules>>();
            }

            return modules;
        }

        public async Task<List<Lectures>> GetLectures()
        {
            List<Lectures> lectures = null;

            HttpResponseMessage responseMessage = await client.GetAsync("api/Lectures/GetAllLectures");

            if(responseMessage.IsSuccessStatusCode)
            {
                lectures = await responseMessage.Content.ReadAsAsync<List<Lectures>>();
            }

            return lectures;
        }

        public async Task<bool> UpdateStudent(int id, Student student)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/Students/UpdateStudent/{id}", student);

            if(responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> UpdateCourse(int id, Courses course)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/Course/UpdateCoure/{id}", course);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<List<Courses>> DeleteCourse(int id)
        {
            List<Courses> course = null;

            HttpResponseMessage responseMessage = await client.DeleteAsync($"api/Course/RemoveCourse/{id}");

            if (responseMessage.IsSuccessStatusCode)
            {
                HttpResponseMessage httpResponseMessage = await client.GetAsync("api/Course/GetAllCourse");
                course = await httpResponseMessage.Content.ReadAsAsync<List<Courses>>();
            }

            return course;
        }

        public async Task<bool> UpdateLectures(int id, Lectures lecture)
        {
            HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/Lectures/UpdateLecture/{id}", lecture);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateStudent(Student student)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Students/CreateStudent", student);

            if(responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateLecture(Lectures lectures)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Lectures/CreateLectures", lectures);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateModules(Modules modules)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Subjects/AddSubjects", modules);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CreateCourse(Courses courses)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Course/AddCourse", courses);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }


    }
}
