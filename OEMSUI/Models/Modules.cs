﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEMSUI.Models
{
    public class Modules
    {
        public int ModuleId { get; set; }

        public string ModuleName { get; set; }

        public int CourseId { get; set; }
        public Courses Courses { get; set; }

        public List<Marks> Marks { get; set; }
    }
}
