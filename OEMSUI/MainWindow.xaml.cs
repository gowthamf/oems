﻿using OEMSUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEMSUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        APIClient client = new APIClient();
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void BtnLogin_ClickAsync(object sender, RoutedEventArgs e)
        {
            var passoword = txtPassWord.Password.ToString();
            var userName = txtUserName.Text;
            Users user= await client.LoginAsync(userName, passoword);
            if(user==null)
            {
                MessageBox.Show("Incorrect User Name or Password");
                return;
            }
            var main = new Main(user);
            main.Show();
            this.Close();
        }
    }
}
