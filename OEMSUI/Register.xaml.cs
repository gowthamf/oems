﻿using OEMSUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OEMSUI
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        public string TabName { get; set; }

        private APIClient _client = new APIClient();
        public Register()
        {
            //InitializeComponent();
        }

        public Register(string tabName, List<Courses>courses,List<Modules> modules)
        {
            InitializeComponent();
            TabName = tabName;

            if (tabName == "Student")
            {
                txtName.Text = "Student Name";
                cmbCourses.ItemsSource = courses;
                txtModule.Visibility = Visibility.Hidden;
                cmbModules.Visibility = Visibility.Hidden;
            }
            else if(tabName=="Module")
            {
                txtName.Text = "Module Name";
                cmbCourses.ItemsSource = courses;
                txtModule.Visibility = Visibility.Hidden;
                cmbModules.Visibility = Visibility.Hidden;
            }
            else if(tabName=="Lecture")
            {
                txtName.Text = "Lecture Name";
                cmbCourses.ItemsSource = courses;
                txtModule.Visibility = Visibility.Hidden;
                cmbModules.Visibility = Visibility.Hidden;
            }
            else if(tabName=="Course")
            {
                txtName.Text = "Course Name";
                txtName1.Text = "Course Name";
                cmbModules.ItemsSource = modules;
                txtCourse.Visibility = Visibility.Hidden;
                cmbCourses.Visibility = Visibility.Hidden;
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if(TabName=="Student")
            {
                Student student = new Student { StudentName = txtName1.Text };
                await _client.CreateStudent(student);
            }
            else if(TabName=="Module")
            {
                var selectedCourse = (Courses)cmbCourses.SelectedItem;
                Modules modules = new Modules { CourseId = selectedCourse.CourseId, Courses = selectedCourse, ModuleName = txtName1.Text };
                await _client.CreateModules(modules);
            }
            else if(TabName=="Lecture")
            {
                Lectures lectures = new Lectures { LectureName = txtName1.Text };
                await _client.CreateLecture(lectures);
            }
            else if(TabName=="Course")
            {
                List<Modules> selectedModules = new List<Modules>();
                selectedModules.Add((Modules)cmbModules.SelectedItem);
                Courses courses = new Courses { CourseName = txtName1.Text, ModulesList = selectedModules };

                await _client.CreateCourse(courses);
            }
        }
    }
}
